import { Page, Locator } from '@playwright/test';
import { expect } from '@playwright/test';

export class RatingPage {
  readonly page: Page;
  readonly errorPopup: Locator;
  readonly likesElements: Locator;

  constructor(page: Page) {
    this.page = page;
    this.errorPopup = page.locator('text=Ошибка загрузки рейтинга');
    this.likesElements = page.locator('td.rating-names_item-count__1LGDH.has-text-success');
  }

  async goto() {
    await this.page.goto('https://meowle.fintech-qa.ru/rating');
  }

  async mockServerError() {
    await this.page.route('https://meowle.fintech-qa.ru/api/likes/cats/rating', route => {
      route.fulfill({
        status: 500,
        contentType: 'application/json',
        body: JSON.stringify({ error: "Internal Server Error" })
      });
    });
  }

  async expectErrorPopupVisible() {
    await expect(this.errorPopup).toBeVisible();
  }

  async getLikesArray(): Promise<number[]> {
    const likesCount = await this.likesElements.count();
    const likesArray = [];
    for (let i = 0; i < likesCount; i++) {
      const likeText = await this.likesElements.nth(i).textContent();
      likesArray.push(parseInt(likeText, 10));
    }
    return likesArray;
  }

  async expectLikesDescending() {
    const likesArray = await this.getLikesArray();
    for (let i = 0; i < likesArray.length - 1; i++) {
      expect(likesArray[i]).toBeGreaterThanOrEqual(likesArray[i + 1]);
    }
  }
}
