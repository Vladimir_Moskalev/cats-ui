import { test, expect } from '@playwright/test';

test.skip('При ошибке сервера в методе rating - отображается попап ошибки', async ({
  page,
}) => {
  await page.route(
    'https://meowle.fintech-qa.ru/api/likes/cats/rating',
    route => {
      route.fulfill({
        status: 500,
        contentType: 'application/json',
        body: JSON.stringify({ error: 'Internal Server Error' }),
      });
    }
  );

  await page.goto('https://meowle.fintech-qa.ru/rating');

  await expect(page.locator('text=Ошибка загрузки рейтинга')).toBeVisible();
});

test.skip('Значения лайков в рейтинге котиков отображаются по убыванию', async ({
  page,
}) => {
  await page.goto('https://meowle.fintech-qa.ru/rating');

  const likesElements = page.locator(
    'td.rating-names_item-count__1LGDH.has-text-success'
  );
  const likesCount = await likesElements.count();

  const likesArray = [];

  for (let i = 0; i < likesCount; i++) {
    const likeText = await likesElements.nth(i).textContent();
    likesArray.push(parseInt(likeText, 10));
  }

  for (let i = 0; i < likesArray.length - 1; i++) {
    expect(likesArray[i]).toBeGreaterThanOrEqual(likesArray[i + 1]);
  }
});
