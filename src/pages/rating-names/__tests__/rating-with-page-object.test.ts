import { test } from '@playwright/test';
import { RatingPage } from '../__page-object__/index';

test.describe('Рейтинг котиков', () => {
  test('При ошибке сервера в методе rating - отображается попап ошибки', async ({ page }) => {
    const ratingPage = new RatingPage(page);
    
    await ratingPage.mockServerError();
    await ratingPage.goto();
    await ratingPage.expectErrorPopupVisible();
  });

  test('Значения лайков в рейтинге котиков отображаются по убыванию', async ({ page }) => {
    const ratingPage = new RatingPage(page);

    await ratingPage.goto();
    await ratingPage.expectLikesDescending();
  });
});
